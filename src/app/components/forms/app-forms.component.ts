import { ChangeDetectionStrategy, Component, EventEmitter, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { UserService } from "src/app/services/user.service";
import { correctLength } from "src/app/validators/correctLength.validator";
import { passConfirm, setCorrectPassword } from "src/app/validators/passwordConfirm.validator";


@Component({
  selector: "app-forms",
  templateUrl: "./app-forms.component.html",
  styleUrls: ["./app-forms.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormsComponent implements OnInit {

  form: FormGroup
  password=""

  constructor(public formBuilder: FormBuilder, private router: Router,
    private userService: UserService) { }

  onSignUp: EventEmitter<string>=new EventEmitter()

  ngOnInit() {
    this.form=this.formBuilder.group({
      account: new FormGroup({
        email: new FormControl("", [Validators.required, Validators.email]),
        password1: new FormControl("", [Validators.required, Validators.minLength(6)]),
        password2: new FormControl("", [Validators.required, passConfirm()])
      },
      ),
      profile: new FormGroup({
        name: new FormControl("", [Validators.required]),
        phone: new FormControl("", [Validators.required, Validators.minLength(11)]),
        city: new FormControl("", [Validators.required])
      }),
      company: new FormGroup({
        compName: new FormControl("", [Validators.required]),
        formOwnership: new FormControl("", Validators.required),
        inn: new FormControl("", [Validators.required, correctLength(9)]),
        okpo: new FormControl("", [Validators.required, correctLength(8)]),
        date: new FormControl("", [Validators.required]),
      }),
      contacts: new FormArray([])
    },)
  }


  addContact() {
    const control=new FormGroup({
      name: new FormControl("", [Validators.required]),
      position: new FormControl("", [Validators.required]),
      phone: new FormControl("", [Validators.required, Validators.minLength(11)])
    });
    (this.form.get("contacts") as FormArray).push(control)
  }

  get contacts(): FormArray {
    return this.form.get("contacts") as FormArray;
  }

  sendPass() {
    const password=this.form.controls["account"].value["password1"]
    setCorrectPassword(password)
  }

  checkForm(event: Event) {
    const value=(event.target as HTMLInputElement).value;

    if (value==="llc") {
      this.addKpp()
    } else if (value==="ip" && this.form.get("company.kpp")) {
      this.removeKpp()
    }
  }

  addKpp(): any {
    (this.form.get("company") as FormGroup).addControl("kpp", this.formBuilder.control("", [Validators.required, correctLength(9)]))
  }

  removeKpp(): any {
    (this.form.get("company") as FormGroup).removeControl("kpp")
  }

  signUp() {
    this.userService.saveData(this.form)
    this.router.navigate(["/sent"])
  }

}
