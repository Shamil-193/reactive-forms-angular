import { ChangeDetectionStrategy, Component, DoCheck, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { UserService } from "src/app/services/user.service";
import { setCorrectPassword } from "src/app/validators/passwordConfirm.validator";

@Component({
  selector: "user-page",
  templateUrl: "./user-page.component.html",
  styleUrls: ["./user-page.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserPageComponent implements OnInit, DoCheck {

  form: FormGroup
  inputForm: FormGroup

  constructor(private userService: UserService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.form=this.formBuilder.group({
      account: new FormGroup({
        email: new FormControl("", []),
        password1: new FormControl("", []),
        password2: new FormControl("", [])
      },
      )
    });
  }

  ngDoCheck(): void {
    this.form=this.userService.getData();
  }

  get contacts(): FormArray {
    return this.form.get("contacts") as FormArray;
  }

  sendPass() {
    const password=this.form.controls["account"].value["password1"]
    setCorrectPassword(password)
  }

}
