import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms"

export function correctLength(length: number): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        if (control.value.length !== length) {
            return {
                incorrectLength: true
            }
        }
        return null
    }
}