import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms"

export let correctPassword: string

export function passConfirm(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        if (control.value !== correctPassword) {
            return {
                incorrectPass: true
            }
        }
        return null
    }
}

export function setCorrectPassword(inputPassword: string) {
    correctPassword=inputPassword
}