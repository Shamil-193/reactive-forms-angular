import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";

@Injectable({
  providedIn: "root"
})
export class UserService {

  form: FormGroup

  saveData(data:FormGroup){
    this.form=data
  }

  getData(){
    return this.form
  }
}
