import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FormsComponent } from "./components/forms/app-forms.component";
import { UserPageComponent } from "./components/user-page/user-page.component";

const routes: Routes = [
  { path: "", component: FormsComponent },
  { path: "sent", component: UserPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
